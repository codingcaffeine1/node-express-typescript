import type {Config} from '@jest/types';

// Objet synchrone
const config: Config.InitialOptions = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  verbose: true,
};
export default config;